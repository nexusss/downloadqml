#include <QApplication>
#include <QQmlApplicationEngine>
#include <mainclass.h>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    MainClass w(engine);
    return app.exec();
}
