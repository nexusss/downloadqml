#ifndef DOWNLOADCLASS_H
#define DOWNLOADCLASS_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QFile>

class downloadClass : public QObject
{
    Q_OBJECT
    QNetworkAccessManager *manager;
    QNetworkReply *reply;

    QFile *file;
    QUrl url;
    QTime timeElapsed;
    void downLoadProgress(QVariant currentSize,QVariant totalSize);
public:
    explicit downloadClass(QObject *parent = 0);
    ~downloadClass();

signals:
    void downloadFinished();
    void updateDownloadProgress(QVariant,QVariant,QVariant);
    void updateTimeLeft(QVariant);


public slots:
    void downLoadClick();
    void httpDownloadFinished();
    void httpReadyRead();
    void stopDownLoad();

    void setUrl(QString url);

};

#endif // DOWNLOADCLASS_H
