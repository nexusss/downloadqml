#include "downloadclass.h"
#include <QFileInfo>
#include <QMessageBox>
#include <QTime>

downloadClass::downloadClass( QObject *parent) : QObject(parent),file(0)
{
manager = new QNetworkAccessManager(this);
reply = 0;
}



void downloadClass::setUrl(QString url){
    this->url.setUrl(url);

}

void downloadClass::downLoadClick(){

    if(!url.isValid()){
        QMessageBox::information(0, tr("HTTP"),
                      tr("Url not valid"));
        return;
    }

    QFileInfo fileInfo(url.path());
        QString fileName = fileInfo.fileName();

        if (fileName.isEmpty())
            fileName = "index.html";

        if (QFile::exists(fileName)) {
                if (!QMessageBox::question(0, tr("HTTP"),
                        tr("There already exists a file called %1 in "
                        "the current directory. Overwrite?").arg(fileName))){/*,
                        QMessageBox::Yes|QMessageBox::No, QMessageBox::No)
                        == QMessageBox::No)*/
                        return;
                }
                QFile::remove(fileName);
            }

        if(file)
            delete file;

        file = new QFile(fileName);
            if (!file->open(QIODevice::WriteOnly)) {
                QMessageBox::information(0, tr("HTTP"),
                              tr("Unable to save the file %1: %2.")
                              .arg(fileName).arg(file->errorString()));
                delete file;
                file = 0;
                return;
            }



    reply = manager->get(QNetworkRequest(url));

    connect(reply, SIGNAL(readyRead()),
              this, SLOT(httpReadyRead()));


    connect(reply, &QNetworkReply::downloadProgress,
                   this, &downloadClass::downLoadProgress);

    connect(reply, SIGNAL(finished()),
                      this, SIGNAL(downloadFinished()));

    timeElapsed.start();
}

void downloadClass::downLoadProgress(QVariant currentSize,QVariant totalSize){
    emit updateDownloadProgress(currentSize,totalSize,timeElapsed.elapsed() / 1000.0);
}

void downloadClass::httpReadyRead(){

    if(file)
        file->write(reply->readAll());
}

void downloadClass::httpDownloadFinished(){

    file->close();
    delete file;
    file = 0;
    reply->deleteLater();
    reply = 0;
}

void downloadClass::stopDownLoad(){
    if(reply)
        reply->abort();
}

downloadClass::~downloadClass()
{

    if(file){
        file->close();
        delete file;
    }

    if(reply)
        reply->deleteLater();

}

