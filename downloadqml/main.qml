import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2

ApplicationWindow {
    title: qsTr("Application downloader Version 1.0.0")
    width: 640
    height: 480
    visible: true
    signal startDownloadPressed()
    signal stopDownLoadPressed()
    signal setUrl(string url)

        Rectangle{

            x: 0
            y: 0
            width: 640
            height: 486
            color: "#333333"



            Rectangle {
                id: cancelBut
                x: 20
                y: 437
                width: 75
                height: 23
                color: "gray"
                border.color: "red"
                Text {

                    text: qsTr("Cancel")
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    anchors.fill: parent
                    font.pixelSize: 12
                }
                MouseArea {
                    id: cancelMouseArea
                    anchors.fill: parent
                    onClicked: stopDownLoadPressed()
                }

            }


        Rectangle {
            id: downloadBut
            x: 545
            y: 437
            width: 75
            height: 23
            color: "gray"
            border.color: "light green"
            Text {

                text: qsTr("Download")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                anchors.fill: parent
                font.pixelSize: 12
            }
            MouseArea {
                id: downloadMouseArea
                anchors.fill: parent
                onClicked:  startDownloadPressed()
            }

        }

        ProgressBar {
            id: progressBar
            x: 20
            y: 213
            width: 600
            height: 41
            minimumValue: 0

            Rectangle {
                id: rectangle1
                color: "#ffffff"
                anchors.fill: parent

                Text {
                    id: progressText
                    text: qsTr("")
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    anchors.fill: parent
                    font.pixelSize: 12
                }
            }
        }

        Text {
            id: appNameText
            x: 20
            y: 78
            width: 288
            height: 42
            color: "#ffffff"
            text: qsTr("Application downloader")
            font.pixelSize: 28
        }

        Text {
            id: appVerText
            x: 20
            y: 154
            width: 288
            height: 30
            color: "#ffffff"
            text: qsTr("Version 1.0.0")
            font.pixelSize: 20
        }

        Text {
            id: buttonText
            x: 20
            y: 365
            width: 380
            height: 25
            color: "#ffffff"
            text: qsTr("Click the Download Button below to start downloading")
            font.pixelSize: 16
        }

        Text {
            id: fileSizeLabel
            x: 20
            y: 294
            width: 123
            height: 21
            color: "#ffffff"
            text: qsTr("Download File Size:")
            font.pixelSize: 13
        }

        Text {
            id: sizeText
            x: 149
            y: 294
            width: 108
            height: 21
            color: "#ffffff"
            text: qsTr("")
            font.pixelSize: 12
        }

        TextField {
            id: urlField
            x: 68
            y: 36
            width: 552
            height: 20
            placeholderText: qsTr("")
            onEditingFinished: setUrl(text)
        }

        Text {
            id: text1
            x: 20
            y: 31
            width: 36
            height: 25
            color: "#ffffff"
            text: qsTr("Url")
            font.pixelSize: 24
        }

            }

        function calcTimeLeft(currentSize,totalSize,elapsedTime) {
            var speed = currentSize / elapsedTime;
            var leftSize = totalSize - currentSize;
            var leftTime = leftSize / speed;
            console.log(speed,elapsedTime,leftTime)
            if(leftTime < 60)
                return leftTime + " sec"
            else
                return (leftTime / 60).toFixed(0) + " min"

        }

    function setProgress(current,total,timeElapsed){
        progressBar.value = current
        progressBar.maximumValue = total
        progressText.text = normalizeSize(current) + " / " + normalizeSize(total) + ", " + calcTimeLeft(current,total,timeElapsed)
        sizeText.text = normalizeSize(total)
    }
    function normalizeSize(value){
        var result
        var unitMeas

        if(value > 1024){ //kb
            if(value > 1024 * 1024){//mb
                if(value > 1024 * 1024 * 1024){//gb
                    if(value > 1024 * 1024 * 1024 * 1024){//tb
                        result = value / (1024 * 1024 * 1024 * 1024);
                        unitMeas = " TByte"
                    }
                    else{
                        result = value / (1024 * 1024 * 1024);
                        unitMeas = " GByte"
                    }
                }
                else{
                    result = value / (1024 * 1024);
                    unitMeas = " MByte"
                }
            }
            else{
                result = value / 1024;
                unitMeas = " KByte"
            }
        }
        else{
            result = value ;
            unitMeas = " Byte"
        }


        return result.toFixed(2) + unitMeas;
    }


}
