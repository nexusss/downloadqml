#ifndef MAINCLASS_H
#define MAINCLASS_H

#include <QObject>
#include <QQmlApplicationEngine>

class MainClass : public QObject
{
    Q_OBJECT
    QObject *object;//qml


public:
    explicit MainClass(QQmlApplicationEngine &engine,QObject *parent = 0);

    ~MainClass();

signals:
    void downLoadComplete();



};

#endif // MAINCLASS_H
