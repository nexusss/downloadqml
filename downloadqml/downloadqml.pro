TEMPLATE = app

QT += qml quick network widgets

SOURCES += main.cpp \
    mainclass.cpp \
    downloadclass.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

win32: LIBS += -L$$PWD/../../../../../../myLibs/quazip/lib -lquazip
INCLUDEPATH += $$PWD/../../../../../../myLibs/quazip/include


HEADERS += \
    mainclass.h \
    downloadclass.h

DISTFILES +=
