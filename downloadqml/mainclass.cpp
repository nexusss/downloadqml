#include "mainclass.h"
#include <QNetworkAccessManager>
#include "downloadclass.h"
#include <JlCompress.h>
#include <QDebug>
using namespace std;

MainClass::MainClass(QQmlApplicationEngine &engine, QObject *parent) : QObject(parent)
{


    object = engine.rootObjects().at(0);

    downloadClass *dwncl = new downloadClass(this);


    connect(dwncl, SIGNAL(updateDownloadProgress(QVariant,QVariant,QVariant)),object,SLOT(setProgress(QVariant,QVariant,QVariant)));
    connect(dwncl, SIGNAL(downloadFinished()),object,SLOT(changeToExtractButton()));
    connect(object,SIGNAL(startDownloadPressed()),dwncl,SLOT(downLoadClick()));
    connect(object,SIGNAL(stopDownLoadPressed()),dwncl,SLOT(stopDownLoad()));
    connect(object,SIGNAL(setUrl(QString)),dwncl,SLOT(setUrl(QString)));

}




MainClass::~MainClass()
{

}

